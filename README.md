#README (edit only inside bitbucket editor)  

Looplunch is a simple application for chosing lunch location within a group.  

#Backend installation  
##You need:  
-Nodejs (build with version v6.10.1, npm version 4.4.4)  
-MySql (5.7.16)   

##Run the backend through:  
-`cd server`  
-`npm install` - install server dependencies  
-`node index.js` - server should be up and running  
-create an `env.json` file i.e. `server/setup/env.json` based on `env_template.json`  
-create a `logs` folder i.e. `server/logs`. This folder will contain server logs  

##Development/production build  
-Set your development/production setup by specifying NODE_ENV, you can do it through:  
-Linux and OSX: `export NODE_ENV=production`  
-Windows: `SET NODE_ENV=production`  
-Edit `env.json` in `server/setup/env.json` for different setup  

##env_template  
DB_NAME - your mysql database name / schema  
DB_USER - your mysql username  
DB_PASSWORD - your mysql password  
DB_HOST - your mysql hostname  
SESSION_SECRET - cookie seession, preferable should be a random set of characters,  
    used to encode the cookie. Can be an array of strings, the first secret is used to  
    sign cookies the rest are valid only for decoding, used for secret rotation    
REST_PORT - express port, eg: 4000  
SOCKET_PORT - socke.io port, eg: 4040  
EMAIL   - emailSender config used for stuff like account confirmation  
----FROM - sender email e.g. dineout@dineout.com  
----USER - user for the email used e.g. dineout@dineout.com  
----PASSWORD - the password, obviously  
----HOST - Host name, eg: 127.0.0.1  
----PORT - Email port, eg: 587  


#Frontend installation  
-`cd client`  
-`npm install` - install client dependencies  
-`au run` - run aurelia app  

##Building for production  
-In one step: `npm run build-production`  
-run 'gulp set-prod' - changes env variable to production  
-run 'au build' - prepare JS bundles  
-run 'gulp build' - prepare other files and move everything to server/public  
-run 'gulp set-dev' - resets env variable  

##Licence  
All code is property of property of their respective owners.
