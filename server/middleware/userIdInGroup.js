'use strict'
const GroupModel = require.main.require('./models/GroupModel')
const GroupUserModel = require.main.require('./models/GroupUserModel')
const logger = require.main.require('./setup/logger')

function userIdInGroup (req, res, next) {
  const groupId = req.params.groupId
  const userId = req.session.passport.user

  GroupModel.findOne({
    where: { id: groupId },
    include: {
      model: GroupUserModel,
      where: { userId }
    }
  }).then(data => {
    if (!data) {
      logger.info('Security: Unauthorized access - someone tries to gain information from outside of his/her group')
      return res.send('Unauthorized access')
    }
    next()
  }).catch(err => {
    next(err)
  })
}

module.exports = userIdInGroup
