'use strict'
const logger = require.main.require('./setup/logger')
const isProduction = require.main.require('./setup/serverConfig').isProduction()

function errorHandler (err, req, res, next) {
  logger.error(err)
  if (isProduction) { return res.status(500).send('Internal Server Error') }
  if (!isProduction) { return res.status(500).json(err) }
}

module.exports = errorHandler
