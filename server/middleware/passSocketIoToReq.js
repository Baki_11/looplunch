'use strict'
function passSocketIoToReq (io) {
  return function (req, res, next) {
    req.io = io
    next()
  }
}

module.exports = passSocketIoToReq
