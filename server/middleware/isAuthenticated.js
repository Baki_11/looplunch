'use strict'
function isAuthenticated (req, res, next) {
  if (!req.isAuthenticated()) {
    return res.send('Unauthorized access')
  }
  next()
}

module.exports = isAuthenticated
