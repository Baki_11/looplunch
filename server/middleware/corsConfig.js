'use strict'
const isProduction = require.main.require('./setup/serverConfig').isProduction()

function corsConfig (req, res, next) {
  if (!isProduction) {
    res.header('Access-Control-Allow-Origin', 'http://localhost:9000')
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    res.header('Access-Control-Allow-Headers', 'Content-Type')
    res.header('Access-Control-Allow-Credentials', true)
  }
  next()
}

module.exports = corsConfig
