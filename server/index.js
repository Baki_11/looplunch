'use strict'
const bodyParser = require('body-parser')
const express = require('express')
const http = require('http')
const morgan = require('morgan')
const passport = require('passport')
const path = require('path')
const serverConfig = require('./setup/serverConfig').serverConfig()
const socketIo = require('socket.io')

const connection = require('./setup/connection')
const corsConfig = require('./middleware/corsConfig')
const errorHandler = require('./middleware/errorHandler')
const groupSocket = require('./services/groupSocket')
const isAuthenticated = require('./middleware/isAuthenticated')
const logger = require('./setup/logger')
const modelRelations = require('./models/modelRelations')
const passSocketIoToReq = require('./middleware/passSocketIoToReq')
const sessionConfig = require('./setup/sessionConfig')
const timeJobClearGroupUsersVotingFor = require('./services/timeJobClearGroupUsersVotingFor')

const app = express()
const server = http.Server(app)
const io = socketIo(server)

app.use(morgan('combined'))
app.use(corsConfig)
app.use(sessionConfig)
app.use(passSocketIoToReq(io))
app.use(passport.initialize())
app.use(passport.session())
app.use(express.static(path.join(__dirname, '/public')))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.use('/verify', require('./routes/verify/verify'))
app.use('/authentication', require('./routes/authentication/authentication'))
app.use('/groups', isAuthenticated, require('./routes/groups/groups'))
app.use('/users', require('./routes/users/users'))

app.use(errorHandler)

modelRelations()
groupSocket.createInitialGroupSockets(io)
timeJobClearGroupUsersVotingFor.createInitialJobs()
server.listen(serverConfig.SOCKET_PORT)

// Drop the DB by replacing connection.sync() with connection.sync({force: true})
connection.sync().then(() => {
  app.listen(serverConfig.REST_PORT, () => {
    logger.info(`app listening on port ${serverConfig.REST_PORT}`)
  })
})
