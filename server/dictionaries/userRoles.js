'use strict'
const USER_ROLES = {
  admin: 'Admin',
  user: 'User'
}

module.exports = USER_ROLES
