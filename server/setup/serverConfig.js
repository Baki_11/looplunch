'use strict'
const env = require('./env.json')

const NODE_ENV = process.env.NODE_ENV || 'development'

function serverConfig () { return env[NODE_ENV] }
function isProduction () { return NODE_ENV === 'production' }

module.exports.serverConfig = serverConfig
module.exports.isProduction = isProduction
