'use strict'
const Sequelize = require('sequelize')
const serverConfig = require.main.require('./setup/serverConfig').serverConfig()

const connection = new Sequelize(serverConfig.DB_NAME, serverConfig.DB_USER, serverConfig.DB_PASSWORD, {host: serverConfig.DB_HOST})

module.exports = connection
