'use strict'
const serverConfig = require.main.require('./setup/serverConfig').serverConfig()
const session = require('express-session')
const MemoryStore = require('session-memory-store')(session)

const THREE_DAYS = 259200000

const sessionConfig = session({
  secret: serverConfig.SESSION_SECRET,
  resave: false,
  saveUninitialized: false,
  cookie: {
    maxAge: THREE_DAYS,
    httpOnly: true
    // TODO: uncomment the code below only when HTTPS is enabled
    // secure: true,
  },
  store: new MemoryStore()
})

module.exports = sessionConfig
