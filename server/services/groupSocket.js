'use strict'
const GroupModel = require.main.require('./models/GroupModel')
const logger = require.main.require('./setup/logger')

let groupList = {}

const groupSocket = {
  createInitialGroupSockets: createInitialGroupSockets,
  getAll: getAll,
  addGroup: addGroup,
  getGroup: getGroup
}

function createInitialGroupSockets (io) {
  GroupModel.findAll().then(data => {
    data.map(group => {
      addGroup(group.dataValues.id, io.of(group.dataValues.id))
    })
  }).catch(err => {
    logger.error(err)
  })
}

function getAll () {
  return groupList
}

function addGroup (id, socket) {
  if (groupList[id]) { return }
  groupList[id] = {
    socket: socket,
    users: {}
  }

  groupList[id].socket.on('connection', socket => {
    logger.info('someone connected to group with id: ', id)

    const username = socket.handshake.query.username
    const userId = socket.handshake.query.userId

    if (!groupList[id].users[userId]) {
      groupList[id].users[userId] = username
      groupList[id].socket.emit('USER_LIST_CHANGED', groupList[id].users)
    }

    socket.on('disconnect', () => {
      logger.info('someone disconnect from group with id: ', id)

      if (groupList[id].users[userId]) {
        delete groupList[id].users[userId]
        groupList[id].socket.emit('USER_LIST_CHANGED', groupList[id].users)
      }
    })
  })
}

function getGroup (id) {
  return groupList[id].socket
}

module.exports = groupSocket
