'use strict'
const schedule = require('node-schedule')

const GroupModel = require.main.require('./models/GroupModel')
const GroupUserModel = require.main.require('./models/GroupUserModel')
const logger = require.main.require('./setup/logger')

let groupList = {}

const timeJobClearGroupUsersVotingFor = {
  addJob: addJob,
  createInitialJobs: createInitialJobs,
  removeJob: removeJob
}

function addJob (groupId, resetTime) {
  if (!groupId || groupList[groupId]) {
    logger.warn('timeJobClearGroupUsersVotingFor.addJob() - no groupId was given or a groupAlready exists in groupList')
    return
  }
  const resetTimeMinutes = resetTime.split(':')[1]
  const resetTimeHours = resetTime.split(':')[0]
  const cronTime = `${resetTimeMinutes} ${resetTimeHours} * * *`
  const job = createScheduleJob(cronTime)
  groupList[groupId] = job

  logger.info('timeJobClearGroupUsersVotingFor.addJob() - new group added')
  logger.debug('timeJobClearGroupUsersVotingFor.addJob() - cromTime ', cronTime)
}

function createInitialJobs () {
  GroupModel.findAll().then(response => {
    response.map(group => {
      addJob(group.dataValues.id, group.dataValues.resetTime)
    })
  }).catch(err => {
    logger.error(err)
  })
}

function removeJob (groupId) {
  if (!groupId || groupList[groupId]) {
    logger.warn('timeJobClearGroupUsersVotingFor.removeJob() - no groupId was given or a groupAlready exists in groupList')
    return
  }
  groupList[groupId].cancel()
  delete groupList[groupId]
}

function createScheduleJob (cronTime) {
  return schedule.scheduleJob(cronTime, () => {
    GroupUserModel.update({
      votingFor: null
    }, {
      where: { groupId: 1 }
    }).then(usersUpdated => {
      logger.info('cron task lunched: timeJobClearGroupUsersVotingFor.addJob() - usersUpdated ', usersUpdated)
    }).catch(err => {
      logger.error(err)
    })
  })
}

module.exports = timeJobClearGroupUsersVotingFor
