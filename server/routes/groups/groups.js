'use strict'
const express = require('express')

const GroupModel = require.main.require('./models/GroupModel')
const groupSocket = require.main.require('./services/groupSocket')
const GroupUserModel = require.main.require('./models/GroupUserModel')
const logger = require.main.require('./setup/logger')
const timeJobClearGroupUsersVotingFor = require.main.require('./services/timeJobClearGroupUsersVotingFor')
const UserModel = require.main.require('./models/UserModel')
const userRoles = require.main.require('./dictionaries/userRoles')

let router = express.Router()

router.use('/restaurants', require('./restaurants/restaurants'))
router.use('/groupUsers', require('./groupUsers/groupUsers'))

router.get('/', (req, res, next) => {
  const userId = req.session.passport.user

  logger.info(`GET, groups, userId hidden`)
  GroupUserModel.aggregate('groupId', 'DISTINCT', {
    plain: false,
    where: { userId },
    include: { model: GroupModel }
  }).then(data => {
    const mappedResponse = data.map(group => {
      return Object.assign({}, {
        groupName: group['group.groupName'],
        groupId: group['group.id']
      })
    })

    res.json(mappedResponse)
    logger.info('GET, groups, group found')
    logger.debug('GET, groups, ', mappedResponse)
  }).catch(err => {
    next(err)
  })
})

router.post('/', (req, res, next) => {
  const groupName = req.body.groupName
  const resetTime = req.body.resetTime
  const userId = req.session.passport.user

  logger.info('POST, groups, %s, userId hidden', groupName, resetTime)
  if (!groupName || !resetTime) {
    return res.send('No groupName or resetTime  was given')
  }
  GroupModel.create({ groupName, resetTime }).then(group => {
    return UserModel.findById(userId).then((foundUser) => {
      return GroupUserModel.create({
        userId: userId,
        userEmail: foundUser.email,
        role: 'Admin',
        groupId: group.dataValues.id
      }).then(() => {
        groupSocket.addGroup(group.id, req.io.of(group.id))
        timeJobClearGroupUsersVotingFor.addJob(group.id, resetTime)
        res.json(group)
        logger.info('POST, groups, group added')
        logger.debug('POST, groups, ', group.dataValues)
      })
    })
  }).catch(err => {
    next(err)
  })
})

router.put('/:groupId', (req, res, next) => {
  const groupId = req.params.groupId
  const resetTime = req.body.resetTime
  const userId = req.session.passport.user

  logger.info('PUT, groups/:groupId %s, %s, userId hidden', groupId, resetTime)
  if (!groupId || !resetTime) {
    return res.send('No group ID or resetTime was given')
  }

  GroupModel.findById(groupId, {
    include: {
      model: GroupUserModel,
      where: { userId }
    }
  }).then(foundGroup => {
    const userRole = foundGroup.dataValues.group_users[0].dataValues.role
    if (userRole !== userRoles.admin) {
      return res.send('User has insufficient privileges')
    }
    return GroupModel.update({ resetTime }, {
      where: { id: groupId }
    }).then(groupUpdated => {
      const mappedResponse = Object.assign({
        groupId: foundGroup.id,
        groupName: foundGroup.groupName,
        resetTime: foundGroup.resetTime
      })
      res.json(mappedResponse)

      logger.info('PUT, groups/:groupId, %s, group updated', groupId)
      logger.debug('PUT, groups/:groupId, ', foundGroup)
    })
  }).catch(err => {
    next(err)
  })
})

module.exports = router
