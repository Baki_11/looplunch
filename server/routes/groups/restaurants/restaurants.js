'use strict'
const express = require('express')

const GroupModel = require.main.require('./models/GroupModel')
const groupSocket = require.main.require('./services/groupSocket')
const logger = require.main.require('./setup/logger')
const RestaurantModel = require.main.require('./models/RestaurantModel')
const userIdInGroup = require.main.require('./middleware/userIdInGroup')

let router = express.Router()

router.delete('/:groupId', userIdInGroup, (req, res, next) => {
  const groupId = req.params.groupId
  const restaurantId = req.body.restaurantId

  logger.info('DELETE, groups/restaurants/:groupId ', groupId, restaurantId)
  if (!groupId || !restaurantId) {
    return res.send('No group ID or restaurant name was given')
  }

  RestaurantModel.destroy({
    where: {
      groupId: groupId,
      id: restaurantId
    }
  }).then(() => {
    res.json({ restaurantId })
    logger.info('DELETE, groups/groupUsers/:groupId, restaurant deleted')
    logger.debug('DELETE, groups/groupUsers/:groupId ', restaurantId)
    groupSocket.getGroup(groupId).emit('RESTAURANTS_CHANGED')
  }).catch(err => {
    next(err)
  })
})

router.get('/:groupId', userIdInGroup, (req, res, next) => {
  const groupId = req.params.groupId

  logger.info('GET, groups/restaurants/:groupId ', groupId)
  if (!groupId) {
    return res.send('No group ID was given')
  }

  GroupModel.findOne({
    where: { id: groupId },
    include: { model: RestaurantModel }
  }).then(data => {
    if (!data) {
      return res.json([])
    }

    const mappedResponse = Object.assign({}, {
      id: data.dataValues.id,
      groupName: data.dataValues.groupName,
      restaurants: data.dataValues.restaurants.map(restaurant => {
        return Object.assign({}, {
          restaurantName: restaurant.restaurantName
        })
      })
    })

    res.json(mappedResponse)
    logger.info('GET, groups/groupUsers/:groupId, restaurant found')
    logger.debug('GET, groups/groupUsers/:groupId, ', mappedResponse)
  }).catch(err => {
    next(err)
  })
})

router.post('/:groupId', userIdInGroup, (req, res, next) => {
  const groupId = req.params.groupId
  const restaurantName = req.body.restaurantName

  logger.info('POST, groups/restaurants/:groupId ', groupId, restaurantName)
  if (!groupId || !restaurantName) {
    return res.send('No group ID or restaurant name was given')
  }

  RestaurantModel.findOrCreate({
    where: {
      restaurantName: restaurantName,
      groupId: groupId
    }
  }).then(restaurant => {
    const mappedResponse = Object.assign({}, {
      restaurant: restaurant[0],
      duplicate: !restaurant[1]
    })

    res.json(mappedResponse)
    groupSocket.getGroup(groupId).emit('RESTAURANTS_CHANGED')
    logger.info('POST, groups/groupUsers/:groupId, restaurant posted')
    logger.debug('POST, groups/groupUsers/:groupId, ', restaurant.dataValues)
  }).catch(err => {
    next(err)
  })
})

router.put('/:groupId', userIdInGroup, (req, res, next) => {
  const groupId = req.params.groupId
  const restaurantId = req.body.restaurantId
  const restaurantName = req.body.restaurantName

  logger.info('PUT, groups/restaurants/:groupId ', groupId, restaurantId, restaurantName)
  if (!groupId || !restaurantId) {
    return res.send('No group ID or restaurant name was given')
  }

  RestaurantModel.update({ restaurantName }, {
    where: {
      groupId: groupId,
      id: restaurantId
    }
  }).then(() => {
    res.json({ groupId, restaurantId, restaurantName })
    groupSocket.getGroup(groupId).emit('RESTAURANTS_CHANGED')
    logger.info('PUT, groups/groupUsers/:groupId, restaurant updated')
    logger.debug('PUT, groups/groupUsers/:groupId, ', groupId, restaurantId, restaurantName)
  }).catch(err => {
    next(err)
  })
})

module.exports = router
