'use strict'
const express = require('express')

const GroupModel = require.main.require('./models/GroupModel')
const groupSocket = require.main.require('./services/groupSocket')
const GroupUserModel = require.main.require('./models/GroupUserModel')
const logger = require.main.require('./setup/logger')
const userIdInGroup = require.main.require('./middleware/userIdInGroup')
const UserModel = require.main.require('./models/UserModel')

let router = express.Router()
const userRoles = require.main.require('./dictionaries/userRoles')

router.get('/:groupId/self', (req, res, next) => {
  const groupId = req.params.groupId

  logger.info(`GET, groups/groupUsers/:groupId/self', ${groupId}`)
  if (!groupId) { return res.send('No group ID was given') }

  const userId = req.session.passport.user

  GroupModel.findOne({
    where: { id: groupId },
    include: {
      model: GroupUserModel,
      include: UserModel,
      where: { userId }
    }
  }).then(data => {
    if (!data) { return res.send('This user is not part of this group') }

    const mappedResponse = Object.assign({}, {
      id: data.dataValues.id,
      groupName: data.dataValues.groupName,
      user: {
        role: data.dataValues.group_users[0].role,
        name: data.dataValues.group_users[0].user.name,
        userId: data.dataValues.group_users[0].userId,
        votingFor: data.dataValues.group_users[0].votingFor
      }
    })

    res.json(mappedResponse)
  }).catch(err => {
    next(err)
  })
})

router.get('/:groupId', userIdInGroup, (req, res, next) => {
  const groupId = req.params.groupId

  logger.info('GET, groups/groupUsers/:groupId ', groupId)
  if (!groupId) { return res.send('No group ID was given') }

  GroupModel.findOne({
    where: { id: groupId },
    include: {
      model: GroupUserModel,
      include: UserModel
    }
  }).then(data => {
    if (!data) {
      return res.json([])
    }

    const mappedResponse = Object.assign({}, {
      id: data.dataValues.id,
      groupName: data.dataValues.groupName,
      users: data.dataValues.group_users.map(groupUser => {
        const groupUserName = groupUser.user ? groupUser.user.dataValues.name : null
        return Object.assign({}, {
          role: groupUser.role,
          name: groupUserName,
          userId: groupUser.userId,
          votingFor: groupUser.votingFor
        })
      })
    })

    res.json(mappedResponse)
    logger.info('GET, groups/groupUsers/:groupId, group found')
    logger.debug('GET, groups/groupUsers/:groupId, ', mappedResponse)
  }).catch(err => {
    next(err)
  })
})

// Add a user to groupUsers with the given email, if there is a user
// with the given email, also add his/her email to groupUsers
router.post('/:groupId', userIdInGroup, (req, res, next) => {
  const groupId = req.params.groupId
  const userEmail = req.body.userEmail

  logger.info(`POST, groups/groupUsers/:groupId, %s, userEmail hidden`, groupId)
  if (!groupId || !userEmail) {
    return res.send('No group ID or user name was given')
  }

  UserModel.findOne({
    where: { email: userEmail }
  }).then(foundUser => {
    let createModel = { userEmail, groupId }
    if (foundUser) {
      createModel.userId = foundUser.dataValues.id
    }

    return GroupUserModel.findOrCreate({
      where: createModel
    }).then(user => {
      const mappedResponse = Object.assign({}, {
        user: user[0],
        duplicate: !user[1]
      })

      res.json(mappedResponse)
      groupSocket.getGroup(groupId).emit('GROUP_USERS_CHANGED')
      logger.info('POST, groups/groupUsers/:groupId, user added')
      logger.debug('POST, groups/groupUsers/:groupId ', user.dataValues)
    })
  }).catch(err => {
    // TODO: find a way to catch error when userGroup does not exist in DB
    next(err)
  })
})

router.put('/:groupId', (req, res, next) => {
  const groupId = req.params.groupId
  const userId = req.session.passport.user
  const role = req.body.role
  const votingFor = req.body.votingFor
  const requestedUserToUpdateId = req.body.userToUpdateId

  logger.info('PUT, groups/groupUsers/:groupId, %s, userId hidden, %s, %s, userToUpdateId hidden', groupId, role, votingFor)
  if (!groupId && !userId && (!role || !votingFor)) {
    return res.send('No group ID, user ID, role or votingFor value was given')
  }

  let userToUpdateId
  let updateVlues = {}
  if (votingFor) { updateVlues.votingFor = votingFor }

  GroupUserModel.findOne({
    where: { userId, groupId }
  }).then(foundUser => {
    if (role && requestedUserToUpdateId && foundUser.dataValues.role === userRoles.admin) {
      updateVlues.role = role
      userToUpdateId = requestedUserToUpdateId
    } else {
      userToUpdateId = userId
    }

    return GroupUserModel.update(updateVlues, {
      where: {
        groupId: groupId,
        userId: userToUpdateId
      }
    }).then(() => {
      res.json({ groupId, userId, votingFor, role })
      groupSocket.getGroup(groupId).emit('GROUP_USERS_CHANGED')
      logger.info(`PUT, groups/groupUsers/:groupId, user updated`)
      logger.debug('PUT, groups/groupUsers/:groupId, ', groupId, userId, votingFor, role)
    })
  }).catch(err => {
    next(err)
  })
})

module.exports = router
