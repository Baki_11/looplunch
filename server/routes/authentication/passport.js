'use strict'
const bcrypt = require('bcrypt-nodejs')
const LocalStrategy = require('passport-local').Strategy
const passport = require('passport')

const UserModel = require.main.require('./models/UserModel')

passport.use(new LocalStrategy(authenticate))

function authenticate (email, password, done) {
  UserModel.findOne({
    where: { email }
  }).then(user => {
    if (!user || !bcrypt.compareSync(password, user.dataValues.password) || !user.active) {
      return done(null, false, { message: 'Invalid credentials' })
    }

    done(null, user)
  })
}

passport.serializeUser((user, done) => {
  done(null, user.id)
})

passport.deserializeUser((id, done) => {
  UserModel.findOne({
    where: { id }
  }).then(user => {
    if (!user) { return }
    done(null, user)
  })
})
