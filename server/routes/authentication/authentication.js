'use strict'
const express = require('express')
const passport = require('passport')

const GroupModel = require.main.require('./models/GroupModel')
const GroupUsersModel = require.main.require('./models/GroupUserModel')
const logger = require.main.require('./setup/logger')
const UserModel = require.main.require('./models/UserModel')

require('./passport')

let router = express.Router()

router.post('/login', (req, res, next) => {
  logger.info('POST, authentication/login')

  passport.authenticate('local', (err, user, info) => {
    if (err) { return next(err) }
    if (!user) { return res.json({ success: false, message: info.message }) }
    req.login(user, loginErr => {
      if (loginErr) { return next(loginErr) }
      const userId = req.session.passport.user

      updateUserInfo(res, next, userId)
    })
  })(req, res, next)
})

router.get('/logout', (req, res, next) => {
  logger.info('GET, authentication/logout')

  req.session.destroy(err => {
    if (err) { return res.send('Error while loging out') }
    res.json({ logout: true })
  })
})

router.get('/isAuthenticated', (req, res, next) => {
  logger.info('GET, authentication/isAuthenticated')

  res.json({ isAuthenticated: req.isAuthenticated() })
})

function updateUserInfo (res, next, userId) {
  UserModel.findOne({
    where: { id: userId }
  }).then(foundUser => {
    if (!foundUser) {
      return res.send({ success: false, message: 'No such user!' })
    }
    const lastSelectedGroup = foundUser.lastSelectedGroup

    if (lastSelectedGroup) {
      return res.send({ success: true, message: 'authentication succeeded', lastSelectedGroup })
    }
    const userEmail = foundUser.dataValues.email

    setUserIdIfNull(next, userId, userEmail)
    setLastSelectedGroup(next, userEmail, userId)
      .then(response => {
        res.send(response)
        logger.info('POST, authentication/login, used logged in')
        logger.info('POST, authentication/login, ', response)
      })
  }).catch(err => {
    next(err)
  })
}

function setUserIdIfNull (next, userId, userEmail) {
  GroupUsersModel.update({ userId }, {
    where: {
      userEmail,
      $and: { userId: null }
    },
    include: { model: GroupModel }
  }).then(userGroups => {
    logger.info('User id if null updated')
    logger.debug(userGroups)
  }).catch(err => {
    next(err)
  })
}

function setLastSelectedGroup (next, userEmail, userId) {
  return new Promise((resolve, reject) => {
    GroupUsersModel.findAll({
      where: { userEmail },
      include: { model: GroupModel }
    }).then(userGroups => {
      if (!userGroups[0] || !userGroups[0].dataValues.group) {
        resolve({ success: true, message: 'authentication succeeded' })
        return
      }

      const newLastSelectedGroup = userGroups[0].dataValues.groupId
      return UserModel.update({
        lastSelectedGroup: newLastSelectedGroup
      }, {
        where: { id: userId }
      }).then(userUpdated => {
        resolve({ success: true, message: 'authentication succeeded', lastSelectedGroup: newLastSelectedGroup })
        logger.info('user lastSelectedGroup updated')
        logger.debug(newLastSelectedGroup)
      })
    }).catch(err => {
      next(err)
    })
  })
}

module.exports = router
