'use strict'
const express = require('express')

const logger = require.main.require('./setup/logger')
const UserModel = require.main.require('./models/UserModel')

let router = express.Router()

router.get('/userAccountCreation', (req, res, next) => {
  logger.info('GET, verify/userAccountCreation')

  const activationHash = req.url.split('?')[1]

  if (!activationHash) {
    return res.send('Invalid URL')
  }

  UserModel.update({
    activationHash: null,
    active: true
  }, {
    where: { activationHash }
  }).then(activatedUser => {
    res.send('User activated')
    logger.info('User activated')
    logger.debug(activatedUser)
  }).catch(err => {
    next(err)
  })
})

router.get('/userAccountDeletion', (req, res, next) => {
  logger.info('GET, verify/userAccountDeletion')

  const deletionHash = req.url.split('?')[1]

  if (!deletionHash) {
    return res.send('Invalid URL')
  }

  UserModel.findOne({
    where: { deletionHash }
  }).then(userFound => {
    const today = new Date().getTime()
    const deletionHashExpiration = userFound.deletionHashExpiration.getTime()

    if (today > deletionHashExpiration) {
      return res.send('Link no longer active')
    }

    return UserModel.destroy({
      where: { deletionHash }
    }).then(() => {
      res.send('User deleted')
      logger.info('DELETE, users, user deleted')
      logger.debug('DELETE, users, userId hidden')
    })
  }).catch(err => {
    next(err)
  })
})

module.exports = router
