'use strict'
const bcrypt = require('bcrypt-nodejs')
const express = require('express')

const emailSender = require('./emailSender')
const isAuthenticated = require.main.require('./middleware/isAuthenticated')
const logger = require.main.require('./setup/logger')
const UserModel = require.main.require('./models/UserModel')

let router = express.Router()

router.delete('/', isAuthenticated, (req, res, next) => {
  const userId = req.session.passport.user

  logger.info('DELETE, users, userId hidden')
  if (!userId) {
    return res.send('No userId was given')
  }

  const randomInput = (new Date()).valueOf() + Math.random()
  const deletionHash = bcrypt.hashSync(randomInput)
  const ONE_DAY = 1 * 24 * 3600 * 1000
  const deletionHashExpiration = new Date(Date.now() + ONE_DAY)

  UserModel.update({ deletionHash, deletionHashExpiration }, {
    where: { id: userId }
  }).then(() => {
    res.json({ userId })
    logger.info('DELETE, users, user marked for deleted')
    logger.debug('DELETE, users, userId hidden')
  }).catch(err => {
    next(err)
  })
})

router.post('/', (req, res, next) => {
  const PASSWORD_VALIDATION_REGEX = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$/
  const email = req.body.email
  const name = req.body.name
  const passwordRaw = req.body.password

  logger.info('POST, users, email hidden, name hidden, password hidden')
  if (!email || !name || !passwordRaw) {
    return res.send({error: 'No email, name or password was given'})
  }

  if (!PASSWORD_VALIDATION_REGEX.test(passwordRaw)) {
    return res.send({error: 'Password needs to have at least 8 characters, one upper case and one lower case letter and a number'})
  }

  const password = bcrypt.hashSync(passwordRaw)
  const randomInput = (new Date()).valueOf() + Math.random()
  const activationHash = bcrypt.hashSync(randomInput)

  UserModel.create({ email, name, password, activationHash }).then(user => {
    emailSender.sendUserAccountCreation(email, activationHash)
    res.json(user.id)
    logger.info('POST, users, user created')
    logger.debug('POST, users, user.id hidden')
  }).catch(err => {
    next(err)
  })
})

router.put('/', isAuthenticated, (req, res, next) => {
  const lastSelectedGroup = req.body.lastSelectedGroup
  const name = req.body.name
  const userId = req.session.passport.user

  logger.info('PUT, users, %s, name hidden, userId hidden, password hidden', lastSelectedGroup)
  if (!name && !lastSelectedGroup) {
    return res.send('No parameter to update was given')
  }

  let updateValues = {}
  if (name) { updateValues.name = name };
  if (lastSelectedGroup) { updateValues.lastSelectedGroup = lastSelectedGroup };

  UserModel.update(updateValues, {
    where: { id: userId }
  }).then(() => {
    res.send('User updated')
    logger.info('PUT, users, users, user updated')
  }).catch(err => {
    next(err)
  })
})

module.exports = router
