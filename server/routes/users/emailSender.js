'use strict'
const nodemailer = require('nodemailer')

const logger = require.main.require('./setup/logger')
const serverConfig = require.main.require('./setup/serverConfig').serverConfig()

const transporter = nodemailer.createTransport({
  port: serverConfig.EMAIL.PORT,
  host: serverConfig.EMAIL.HOST,
  auth: {
    user: serverConfig.EMAIL.USER,
    pass: serverConfig.EMAIL.PASSWORD
  },
  // TODO: this should be remove on production?
  tls: {
    rejectUnauthorized: false
  }
})

function sendUserAccountCreation (to, activationHash) {
  const mailOptions = {
    from: serverConfig.EMAIL.FROM,
    to: to,
    subject: 'Dineout account confirmation',
    text: 'Thank you for signing up, please click on the following link to activate your account: ' +
          `www.${serverConfig.DOMAIN_ADDRESS}/verify/userAccountCreation?${activationHash}`
  }

  send(mailOptions)
}

function sendUserAccountDeletion (to, deletionHash) {
  const mailOptions = {
    from: serverConfig.EMAIL.FROM,
    to: to,
    subject: 'Dineout account deletion',
    text: 'WARNING: THIS ACTION CANNOT BE UNDONE. This link will only be available for 24h. ' +
          'If you are sure you want to delete your account, please click on the following link: ' +
          `www.${serverConfig.DOMAIN_ADDRESS}/verify/userAccountDeletion?${deletionHash} `
  }

  send(mailOptions)
}

function send (mailOptions) {
  logger.debug('sendUserAccountCreation with options: ', mailOptions)

  transporter.sendMail(mailOptions, (err, info) => {
    if (err) {
      return logger.error('emailSender Error: ', err)
    }
    logger.info('Message %s sent: %s', info.messageId, info.response)
  })
}

module.exports.sendUserAccountCreation = sendUserAccountCreation
module.exports.sendUserAccountDeletion = sendUserAccountDeletion
