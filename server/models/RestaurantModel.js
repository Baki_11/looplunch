'use strict'
const Sequelize = require('sequelize')
const connection = require.main.require('./setup/connection')

const RestaurantModel = connection.define('restaurant', {
  restaurantName: {
    allowNull: false,
    type: Sequelize.STRING,
    validate: {
      len: [1, 60]
    }
  }
})

module.exports = RestaurantModel
