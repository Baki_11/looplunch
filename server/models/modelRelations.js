'use strict'
const GroupModel = require('./GroupModel')
const RestaurantModel = require('./RestaurantModel')
const GroupUserModel = require('./GroupUserModel')
const UserModel = require('./UserModel')

function initRelations () {
  GroupModel.hasMany(GroupUserModel)
  GroupModel.hasMany(RestaurantModel)
  GroupUserModel.belongsTo(GroupModel)
  GroupUserModel.belongsTo(UserModel)
  RestaurantModel.belongsTo(GroupModel)
  UserModel.hasMany(GroupUserModel)
};

module.exports = initRelations
