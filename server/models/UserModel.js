'use strict'
const Sequelize = require('sequelize')
const connection = require.main.require('./setup/connection')

const isProduction = require.main.require('./setup/serverConfig').isProduction()
const shouldUserBeActivatedByDefault = !isProduction

const UserModel = connection.define('user', {
  id: {
    defaultValue: Sequelize.UUIDV4,
    primaryKey: true,
    type: Sequelize.UUID,
    unique: true
  },
  name: {
    allowNull: false,
    type: Sequelize.STRING,
    validate: {
      len: [3, 30]
    }
  },
  email: {
    allowNull: false,
    type: Sequelize.STRING,
    unique: true,
    validate: {
      isEmail: true
    }
  },
  password: {
    allowNull: false,
    type: Sequelize.STRING,
    validate: {
      len: [60, 60]
    }
  },
  lastSelectedGroup: Sequelize.STRING,
  active: {
    allowNull: false,
    defaultValue: shouldUserBeActivatedByDefault,
    type: Sequelize.BOOLEAN
  },
  activationHash: Sequelize.STRING,
  deletionHash: Sequelize.STRING,
  deletionHashExpiration: Sequelize.DATE
})

module.exports = UserModel
