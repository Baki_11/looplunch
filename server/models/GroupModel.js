'use strict'
const Sequelize = require('sequelize')
const connection = require.main.require('./setup/connection')

const GroupModel = connection.define('group', {
  groupName: {
    allowNull: false,
    type: Sequelize.STRING,
    validate: {
      len: [1, 60]
    }
  },
  resetTime: {
    allowNull: false,
    type: Sequelize.STRING(5),
    validate: {
      len: [5, 5]
    }
  }
})

module.exports = GroupModel
