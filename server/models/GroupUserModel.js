'use strict'
const Sequelize = require('sequelize')

const connection = require.main.require('./setup/connection')
const userRoles = require.main.require('./dictionaries/userRoles')

const GroupUserModel = connection.define('group_user', {
  userEmail: Sequelize.STRING,
  role: {
    allowNull: false,
    defaultValue: 'User',
    type: Sequelize.ENUM(userRoles.admin, userRoles.user)
  },
  votingFor: {
    defaultValue: null,
    type: Sequelize.STRING
  }
})

module.exports = GroupUserModel
