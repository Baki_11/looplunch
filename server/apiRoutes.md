`:id` stands for groupId
@name stand for request body arguments
#name stand for SOCKET events emited on DB change

--- API ROUTES ---

GET '/groups'
POST '/groups'   @groupName    @resetTime
PUT '/groups/:id'   @resetTime

DELETE: '/groups/restaurants/:id'   @restaurantId   #RESTAURANTS_CHANGED
GET '/groups/restaurants/:id'
POST '/groups/restaurants/:id'   @restaurantName   #RESTAURANTS_CHANGED
PUT: '/groups/restaurants/:id'   @restaurantId, @restaurantName   #RESTAURANTS_CHANGED

GET '/groups/groupUsers/:id/self'  [@userId taken from cookie]
GET '/groups/groupUsers/:id'   [@userId taken from cookie]
POST '/groups/groupUsers/:id'   @userEmail   #GROUP_USERS_CHANGED
PUT '/groups/groupUsers/:id'   @role, @votingFor, userToUpdateId [@userId taken from cookie]   #GROUP_USERS_CHANGED

DELETE: '/users/'   [@userId taken from cookie]
POST '/users'   @name, @email, @password
PUT '/users'   @name, @lastSelectedGroup, [@userId taken from cookie]

GET '/authentication/isAuthenticated'  // returns a boolean with authentication status
GET '/authentication/logout'   // logout user
POST '/authentication/login'   @username, @password, [@userId taken from cookie] //@username is the user's email

GET '/verify/userAccountCreation'  // user account verificaion via email link
GET '/verify/userAccountDeletion'  // user account deletion via email link


--- SOCKET ---
#USER_LIST_CHANGED - fires when a user enter or leaves a group

--- DB model ---
groups: [
    {
        groupName: STRING,
        resetTime: STRING(5)
        restaurants: [
            {
                restaurantName: STRING
            }
        ]
        groupUsers: [
            {
                role: ENUM<String>,
                userName: STRING,
                votingFor: STRING
            }
        ]
    }
]
users: {
    name: STRING,
    email: STRING,
    password: STRING,
    lastSelectedGroup: STRING,
    active: BOOLEAN,
    activationHash: STRING,
    deletionHash: STRING,
    deletionHashExpiration: STRING
}
