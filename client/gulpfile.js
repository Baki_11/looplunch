const gulp = require('gulp');
const clean = require('gulp-clean');
const replace = require('gulp-replace');

gulp.task('build-js', () => {
    return gulp.src('scripts/**.js')
        .pipe(gulp.dest('../server/public/scripts'));
});

gulp.task('build-html', () => {
    return gulp.src('index.html')
        .pipe(gulp.dest('../server/public'));
});

gulp.task('build-assets', () => {
    return gulp.src('src/assets/**/**.*')
        .pipe(gulp.dest('../server/public/src/assets'));
});

gulp.task('clean', () => {
    return gulp.src('../server/public/**/**.*')
        .pipe(clean({force: true}));
});
 
gulp.task('set-prod', () => {
    return gulp.src(['src/env.js'])
        .pipe(replace('production: false', 'production: true'))
        .pipe(gulp.dest('src/'));
});

gulp.task('set-dev', () => {
    return gulp.src(['src/env.js'])
        .pipe(replace('production: true', 'production: false'))
        .pipe(gulp.dest('src/'));
});

gulp.task('build', gulp.series('clean', 'build-js', 'build-html', 'build-assets'));