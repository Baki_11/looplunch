import environment from './environment';

//Configure Bluebird Promises.
Promise.config({
    longStackTraces: environment.debug,
    warnings: {
        wForgottenReturn: false
    }
});

export function configure(aurelia) {
    aurelia.use
      .standardConfiguration()
      .plugin('aurelia-validation')
      .feature('resources')
      .feature('filters')
      .feature('config')
      .feature('components/common');

    if (environment.debug) {
        aurelia.use.developmentLogging();
    }

    if (environment.testing) {
        aurelia.use.plugin('aurelia-testing');
    }

    aurelia.start().then(() => aurelia.setRoot());
}
