/**
 * Api Service
 * Serves as a pipeline for API requests fired from frontend
 * 
 * Public Methods: 
 *   request()
 */

import { inject } from 'aurelia-framework';
import { json, HttpClient } from 'aurelia-fetch-client';
import { Router } from 'aurelia-router';

import AuthService from 'services/auth-service';

import SERVER_CONFIG from 'config/server-config';

@inject(HttpClient, Router, AuthService, SERVER_CONFIG)
export default class ApiService {
    constructor(HttpClient, Router, AuthService, SERVER_CONFIG) {
        this.http = HttpClient;
        this.router = Router;
        this.authService = AuthService;
        this.SERVER_CONFIG = SERVER_CONFIG;
    }

    request() {
        //update authentication status
        this.authService.isAuthenticated();

        const req = _prepareRequest.call(this, arguments);

        const output = this.http.fetch(req.path, req.config)
            .then((res) => {
                return res.json();
            })
            .catch((err) => {
                console.warn(err);
                //this.router.navigateToRoute('landing');
                return err;
            });
        
        return output;
    }
}

/////////////
function _prepareRequest(inputData) {
    const args = Array.prototype.slice.call(inputData);

    const opts = {
        path: `${this.SERVER_CONFIG.host}${inputData[0]}`,
        config: _prepareConfig()
    }

    /**
     * Determine request type
     * Refactor: use something better than arguments  
     */
    function _prepareConfig() {
        if (args.length === 1) {
            return {
                method: 'get'
            }    
        }

        return {
            method: inputData[2] || 'post',
            body: json(inputData[1])
        }
    }

    return opts;
}
