import { inject } from 'aurelia-framework';
import { json } from 'aurelia-fetch-client';

import ApiService from 'services/api-service';

@inject(ApiService)
export default class GroupsService {
    constructor(ApiService) {
        this.api = ApiService;
    }

    postGroup(req) {
        return this.api.request('/groups/', req).then((data) => {
            return data;
        });
    }

    postUserToGroup(req) {
        return this.api.request(`/groups/groupUsers/${req.pathParam}`, req.body).then((data) => {
            return data;
        });
    }

    getGroups() {
        return this.api.request('/groups/').then((data) => {
            return data;
        });
    }
}
