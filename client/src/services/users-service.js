import { inject } from 'aurelia-framework';
import { json } from 'aurelia-fetch-client';

import ApiService from './api-service';

@inject(ApiService)
export default class UsersService {
    constructor(ApiService) {
        this.api = ApiService;
    }

    getSingleUser(pathParam) {
        return this.api.request(`/groups/groupUsers/${pathParam}/self`).then((data) => {
            return data;
        });
    }

    getUsers(pathParam) {        
        return this.api.request(`/groups/groupUsers/${pathParam}`).then((data) => {
            return data;
        });
    }

    registerUser(req) {
        return this.api.request('/users/', req).then((data) => {
            return data;
        });
    }

    loginUser(req) {
        return this.api.request('/authentication/login', req).then((data) => {
            if (!data.success) {
                alert(`Error! ${data.message}`);
            }

            return data;
        });
    }

    logoutUser() {
        return this.api.request('/authentication/logout').then((data) => {
            return data;
        });
    }
}