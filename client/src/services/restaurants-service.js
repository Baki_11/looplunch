import { inject } from 'aurelia-framework';
import { json } from 'aurelia-fetch-client';
import ApiService from './api-service';

@inject(ApiService)
export default class RestaurantsService {
    constructor(ApiService) {
        this.api = ApiService;
    }

    getRestaurants(pathParam) {
        return this.api.request(`/groups/restaurants/${pathParam}`).then((data) => {
            return data;
        });
    }

    postRestaurant(req) {
        return this.api.request(`/groups/restaurants/${req.pathParam}`, req.body).then((data) => {
            return data;
        });
    }

    putVote(req) {
        return this.api.request(`/groups/groupUsers/${req.pathParam}`, req.body, req.method).then((data) => {
            return data;
        });
    }
}
