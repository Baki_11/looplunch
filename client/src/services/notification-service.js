/**
 * Notification Service
 * Used to control notification state across app
 */

export default class NotificationService {
    constructor() {
        this.changed = null;
        this.changeCounter = 0;

        this.config = {
            message: '',
            isVisible: false
        };
    }

    setProps(config) {
        this.changed = this.changeCounter++;

        this.config.message = config.message;
        this.config.isVisible = config.isVisible;
    }
}