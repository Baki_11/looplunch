import { inject } from 'aurelia-framework';
import { json, HttpClient } from 'aurelia-fetch-client';

import SERVER_CONFIG from 'config/server-config';

@inject(HttpClient, SERVER_CONFIG)
export default class AuthService {
    constructor(HttpClient, SERVER_CONFIG) {
        this.http = HttpClient;
        this.SERVER_CONFIG = SERVER_CONFIG;
        this.authenticated = false;
    }

    isAuthenticated() {
        return this.http.fetch(`${this.SERVER_CONFIG.host}/authentication/isAuthenticated`).then((data) => {
            const parsedData = data.json();

            parsedData.then((response) => {
                this.authenticated = response.isAuthenticated;
            })
        });
    }
}
