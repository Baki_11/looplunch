import { inject } from 'aurelia-framework';
import { HttpClient } from 'aurelia-fetch-client';
import { EventAggregator } from 'aurelia-event-aggregator';

import AuthService from 'services/auth-service';

import routesArray from 'config/routes';

@inject(HttpClient, EventAggregator, AuthService)
export class App {
    constructor(HttpClient, EventAggregator, AuthService) {
        this.ea = EventAggregator;
        this.authService = AuthService;
        
        /**
         * Fetch config
         */
        HttpClient.configure(config => {
            config.withDefaults({
                credentials: 'include',
                headers: {
                    'Cookie': 'fetch-default-cookie=test'
                }
            });
        });
    }

    activate() {
        //Check authentication on each route change
        this.ea.subscribe('router:navigation:processing', response => {
            this.authService.isAuthenticated();
        });
    }

    /**
     * Router config
     */
    configureRouter(config) {
        config.title = 'Lunchout';
        config.fallbackRoute('landing');
        config.map(routesArray);
    }
}