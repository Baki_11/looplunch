import { activationStrategy } from 'aurelia-router';

const routesArray = [
    { 
        route: '', 
        moduleId: 'app', 
        redirect: 'landing'
    },
    { 
        route: 'landing', 
        moduleId: './components/views/landing/landing', 
        title: 'Landing', 
        name: 'landing', 
        nav: true,
        activationStrategy: activationStrategy.invokeLifecycle
    },
    { 
        route: 'login', 
        moduleId: './components/views/login/login', 
        title: 'Login', 
        name: 'login', 
        nav: true
    },
    { 
        route: 'register', 
        moduleId: './components/views/register/register', 
        title: 'Register', 
        name: 'register', 
        nav: true
    },
    { 
        route: 'voting', 
        moduleId: './components/views/voting/voting', 
        title: 'Voting', 
        name: 'voting', 
        nav: true
    },
    { 
        route: 'group-select', 
        moduleId: './components/views/group-select/group-select', 
        title: 'Group Select', 
        name: 'group-select', 
        nav: true
    }
];

export default routesArray;