import env from '../env';

const SERVER_CONFIG = {};

if (env.production) {
    Object.assign(SERVER_CONFIG, {    
        host: 'http://looplunch.com', 
        socket: 'http://looplunch.com:4040'
    });
} else {
    Object.assign(SERVER_CONFIG, {    
        host: 'http://localhost:4000', 
        socket: 'http://localhost:4040'
    });
}

export default SERVER_CONFIG;
