export class AlphaSortValueConverter {
    toView(array, config) {
        return array
            .sort((val1, val2) => {
                let a = val1, b = val2;

                if (config.dir !== 'asc') {
                    a = val2;
                    b = val1;
                }

                return a[config.prop].toLowerCase() > b[config.prop].toLowerCase();
            });
    }
}