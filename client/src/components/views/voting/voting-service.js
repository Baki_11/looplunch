import { inject } from 'aurelia-framework';

import RestaurantsService from 'services/restaurants-service';
import UsersService from 'services/users-service';

@inject(RestaurantsService, UsersService)
export default class VotingService {
    constructor(RestaurantsService, UsersService) {
        this.restaurantsService = RestaurantsService;
        this.usersService = UsersService;
    }

    getFullData(selectedGroupId) {
        const allRequests = () => {
            return Promise.all([
                this.getSelf.call(this, selectedGroupId).then((res) => res),
                this.getUserInfo.call(this, selectedGroupId).then((res) => res),
                this.getRestaurantsList.call(this, selectedGroupId).then((res) => res)
            ]);
        }

        return allRequests().then((promiseArr) => {
            const votingData = {};

            Object.assign(votingData, {
                groupName: promiseArr[0].groupName,
                id: promiseArr[0].id,
                user: _getProp(promiseArr, 'user'),
                users: _getProp(promiseArr, 'users'),
                restaurants: _getProp(promiseArr, 'restaurants')
            });

            ///////////
            function _getProp(promiseArr, key) {
                const searchItem = promiseArr.find((item) => {
                    return item[key];
                });

                return searchItem[key];
            }

            return votingData;
        });        
    }

    getSelf(selectedGroupId) {
        return this.usersService.getSingleUser(selectedGroupId);
    }

    getRestaurantsList(selectedGroupId) {
        return this.restaurantsService.getRestaurants(selectedGroupId);
    }

    getUserInfo(selectedGroupId) {
        return this.usersService.getUsers(selectedGroupId);
    }

    addRestaurant(req) {
        return this.restaurantsService.postRestaurant(req);
    }

    voteFor(req) {
        return this.restaurantsService.putVote(req);
    }
}


