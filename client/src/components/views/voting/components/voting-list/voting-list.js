import { bindable } from 'aurelia-framework';

export class VotingList {
    @bindable restaurants;
    @bindable users;
    @bindable self;
    @bindable voteForCallback;

    constructor() {}
}
