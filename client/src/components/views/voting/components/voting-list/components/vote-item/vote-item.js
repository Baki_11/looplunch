import { bindable } from 'aurelia-framework';
import { inject } from 'aurelia-framework';  

import RestaurantsService from 'services/restaurants-service';

@inject(RestaurantsService)
export class VoteItem {
    @bindable item;
    @bindable users;
    @bindable self;
    @bindable voteForCallback;

    constructor(RestaurantsService) {
        this.restaurantsService = RestaurantsService
        this.selectedRestaurant = null;
        
        this.namesList = [];
    }

    attached() {
        //remember checked radio button after page refresh
        this.setRadioBtn();
    }
    
    usersChanged() {
        this.attachNames();
        this.setRadioBtn();
    }

    voteFor() {
        this.voteForCallback({selectedRestaurant: this.selectedRestaurant});
    }

    attachNames() {
        setTimeout(() => {
            this.namesList = [];

            this.users.find((o) => {
                if (o.votingFor === this.item.restaurantName) {
                    this.namesList.push(o.name);
                }
            })
        });
    }

    setRadioBtn() {
        setTimeout(() => {
            if (!this.radioButton) {
                return;
            }

            if (this.radioButton.value === this.self.votingFor) {
                this.radioButton.checked = true;
            }
        })
    }
}
