import { bindable } from 'aurelia-framework';

export class UsersList {
    @bindable users;

    constructor() {
        this.usersList = this.users;
    }

    usersChanged(newVal) {
        this.usersList = newVal;
    }
}
