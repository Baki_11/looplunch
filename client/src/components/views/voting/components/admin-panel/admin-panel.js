import { inject } from 'aurelia-framework';  
import { bindable } from 'aurelia-framework';

import GroupsService from 'services/groups-service';

@inject(GroupsService)
export class AdminPanel {
    @bindable groupId;

    constructor(GroupsService) {
        this.groupsService = GroupsService;
        this.newMemberAdded = false;
        this.newMemberEmail = null;
    }

    attached() {
        //debug component
    }

    addNewMember() {
        const req = {
            pathParam: this.groupId,
            body: {
                userEmail: this.newMemberEmail
            }
        };

        this.groupsService.postUserToGroup(req).then((data) => {
            this.newMemberEmail = '';
            this.newMemberAdded = true;

            setTimeout(() => {
                this.newMemberAdded = false;
            },2000);
        });
    }
}