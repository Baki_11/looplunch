import { inject } from 'aurelia-framework';

import io from 'io';

import RestaurantsService from 'services/restaurants-service';
import UsersService from 'services/users-service';
import VotingService from './voting-service';

import SERVER_CONFIG from 'config/server-config';

@inject(RestaurantsService, UsersService, VotingService, SERVER_CONFIG)
export class RestaurantVoting {
    constructor(RestaurantsService, UsersService, VotingService, SERVER_CONFIG) {
        this.restaurantsService = RestaurantsService;
        this.usersService = UsersService;
        this.votingService = VotingService;
        this.SERVER_CONFIG = SERVER_CONFIG;

        this.votingData = {
            groupName: '',
            id: 0,
            restaurants: [
                {
                    restaurantName: ''
                }
            ],
            user: {
                name: '',
                role: '',
                userId: '',
                votingFor: ''
            },
            users: []
        };

        this.hasRestaurants = false;
        this.chosenRestaurant = this.votingData.user.votingFor;

        this.adminPanelOpen = false;
        this.isAdmin = false;
    }

    //aurelia router lifecycle method called before view-model is displayed
    activate(params) {
        //TODO: pass real username or and id
        const socket = io.connect(`${this.SERVER_CONFIG.socket}/${params.groupId}`, {query:`username=${this.votingData.user.name}&userId=${this.votingData.user.userId}`});
        
        this.getFullData(params.groupId);

        socket.on('RESTAURANTS_CHANGED', () => {
            console.log('--- restaurants changed ---')
            this.getRestaurantsList();
        });

        socket.on('GROUP_USERS_CHANGED', () => {
            console.log('--- group users changed ---')
            this.getFullData(params.groupId);
        });

        socket.on('USER_LIST_CHANGED', () => {
            console.log('--- user list changed ---');
        });

    }

    getFullData(groupId) {
        this.votingService.getFullData(groupId).then((data) => {
            this.votingData = data;

            this.isAdmin = _checkIfAdmin(this.votingData.user.role);
            _prepareRestaurants.call(this, this.votingData.restaurants);

            console.log(this.votingData);
        });
    }

    getRestaurantsList() {
        const pathParam = this.votingData.id;

        this.votingService.getRestaurantsList(pathParam).then((data) => {
            Object.assign(this.votingData, {
                restaurants: data.restaurants
            })
            
            _prepareRestaurants.call(this, this.votingData.restaurants);
        });
    }

    addRestaurant() {
        const req = {
            pathParam: this.votingData.id,
            body: {
                restaurantName: this.chosenRestaurant
            },
            method: 'post'
        };

        //restaurant list will update based on socket.io event
        this.votingService.addRestaurant(req).then(() => {
            this.chosenRestaurant = '';
        });
    }

    voteFor(selectedRestaurant) {
        const req = {
            pathParam: this.votingData.id,
            body: {
                votingFor: selectedRestaurant
            },
            method: 'put'
        };

        //voting list will update based on socket.io event
        this.votingService.voteFor(req);
    }

    getUserInfo() {
        const pathParam = this.votingData.id;

        this.votingService.getUserInfo(pathParam).then((data) => {
            this.votingData.users = data.users;
        });
    }

    getSelf() {
        const pathParam = this.votingData.id;

        this.votingService.getSelf(pathParam).then((data) => {
            this.votingData.user = data.user;
        });
    }

    toggleAdminPanel() {
        this.adminPanelOpen ? this.adminPanelOpen = false : this.adminPanelOpen = true;
    }
}

/////////////////
function _prepareRestaurants(restaurants) {
    if (restaurants.length > 0) {
        this.hasRestaurants = true;
    }

    restaurants.forEach((rstrnt, i) => {
        let counter = 0;

        Object.assign(rstrnt, {voteCount: counter});

        this.votingData.users.forEach((usr, j) => {
            if (rstrnt.restaurantName === usr.votingFor) {
                counter++;
                rstrnt.voteCount = counter;
            }
        })
    });
}

function _checkIfAdmin(role) {
    if (role === 'Admin') {
        return true;
    }

    return false;
}