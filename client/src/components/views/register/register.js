import { Router } from 'aurelia-router';
import { inject } from 'aurelia-framework';  
import { ValidationControllerFactory, ValidationRules } from 'aurelia-validation';

import UsersService from 'services/users-service';
import NotificationService from 'services/notification-service';

@inject(Router, UsersService, NotificationService, ValidationControllerFactory)
export class Register {
    constructor(Router, UsersService, NotificationService, ValidationControllerFactory) {
        this.router = Router;
        this.usersService = UsersService;
        this.notificationService = NotificationService;
        this.validation = ValidationControllerFactory.createForCurrentScope();
        
        this.userData = {
            name: null,
            email: null,
            password: null
        }

        ValidationRules
            .ensure('name')
                .required().withMessage('name is requierd')
            .ensure('email')
                .required().withMessage('email is required')
            .ensure('password')
                .required().withMessage('password is required')
                .matches(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$/).withMessage('Password needs to have at least 8 characters, one uppercase and one lowercase letter and a number')
            .on(this.userData);
    }

    goTo() {
        this.router.navigate('landing');
    }

    register() {
        const req = this.userData;

        this.usersService.registerUser(req).then((data) => {
            if (data.error) {
                alert(data.error);
            } else {
                this.notificationService.setProps({
                    message: `Account succefully created. Please login.`,
                    isVisible: true
                });

                this.router.navigate('login');
            }
        });
    }
}
