import { Router } from 'aurelia-router';
import { inject } from 'aurelia-framework';  

import GroupsService from 'services/groups-service';

@inject(Router, GroupsService)
export class GroupSelect {
    constructor(Router, GroupsService) {
        this.router = Router;
        this.groupsService = GroupsService;

        this.groups = [];
        this.selectedGroup = null;
        this.newGroupAdded = false;
        this.noGroup = true;

        this.getGroupsList();
     }

     goTo() {
         this.router.navigateToRoute('voting', {groupId: this.selectedGroup});
         
     }

     updateSelected(groupId) {
         this.selectedGroup = groupId;
     }

     addNewGroup(groupName) {
         const req = {
             groupName: groupName,
             resetTime: '12:00'
         };

         this.groupsService.postGroup(req).then((data) => {
            this.newGroupAdded = true;
            this.getGroupsList();

            setTimeout(() => {
                this.newGroupAdded = false;
            },2000);
         });
         
     }

     getGroupsList() {
        this.groupsService.getGroups().then((data) => {
            const response = data;
               
            this.groups = response;

            if (this.groups.length > 0) {
                this.noGroup = false;
            }
        });
     }
}
