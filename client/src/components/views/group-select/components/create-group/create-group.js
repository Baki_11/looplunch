import { bindable } from 'aurelia-framework';

export class CreateGroup {
    @bindable addNewGroupCallback
    @bindable newGroupAdded

    constructor() {
        this.groupName = '';
        this.step = 'INFO';
    }

    openForm() {
        this.step = 'FORM';
    }

    closeForm() {
        this.step = 'INFO';
    }

    addNewGroup() {
        this.addNewGroupCallback({groupName: this.groupName});
        this.groupName = '';
    }
}
