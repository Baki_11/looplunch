import { Router } from 'aurelia-router';
import { inject } from 'aurelia-framework';  

@inject(Router)
export class Landing {
    constructor(Router) {
        this.router = Router;
    }

    goTo(route) {
        this.router.navigate(route)
    }

}
