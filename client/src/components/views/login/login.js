import { Router } from 'aurelia-router';
import { inject } from 'aurelia-framework'; 
import { ValidationControllerFactory, ValidationRules } from 'aurelia-validation';

import UsersService from 'services/users-service'; 

@inject(Router, UsersService, ValidationControllerFactory)
export class Login {
    constructor(Router, UsersService, ValidationControllerFactory) {
        this.router = Router;
        this.usersService = UsersService;
        this.validation = ValidationControllerFactory.createForCurrentScope();

        this.userData = {
            username: null,
            password: null
        }


        ValidationRules
            .ensure('username')
                .required().withMessage('email is requierd')
                .email().withMessage('invalid email')
            .ensure('password')
                .required().withMessage('password is required')
                .matches(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$/).withMessage('Password needs to have at least 8 characters, one uppercase and one lowercase letter and a number')
            .on(this.userData);
    }

    goTo() {
        console.log(this);
        //this.router.navigate('landing');
    }

    login(route) {
        const req = this.userData;

        this.usersService.loginUser(req).then((data) => {
            if (data.success) {
                this.router.navigate(route);
            }
        });
    }
}
