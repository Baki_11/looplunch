import { inject } from 'aurelia-framework'; 
import { BindingEngine } from 'aurelia-binding';

import NotificationService from 'services/notification-service';

@inject(BindingEngine, NotificationService)
export class Notification {
    constructor(BindingEngine, NotificationService) {
        this.notificationService = NotificationService;
        this.bindingEngine = BindingEngine;

        //default config
        this.message = '';
        this.isVisible = false;

        const sub = this.bindingEngine.propertyObserver(this.notificationService, 'changed')
            .subscribe(() => {
                this.message = this.notificationService.config.message;
                this.isVisible = this.notificationService.config.isVisible;
            });
    }

    close() {
        this.isVisible ? this.isVisible = false : this.isVisible = true;
    }
}