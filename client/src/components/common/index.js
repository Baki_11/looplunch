export function configure(config) {
    config.globalResources(
        [
            './app-header/app-header', 
            './notification/notification', 
            './error-messages/error-messages',
            './form-validation/form-validation'
        ]
    );
}
