import { inject } from 'aurelia-framework';
import { Router } from 'aurelia-router';
import { BindingEngine } from 'aurelia-binding';

import UsersService from 'services/users-service';
import AuthService from 'services/auth-service';
import NotificationService from 'services/notification-service';

@inject(UsersService, AuthService, NotificationService, Router, BindingEngine)
export class AppHeader {
    constructor(UsersService, AuthService, NotificationService, Router, BindingEngine) {
        this.usersService = UsersService;
        this.authService = AuthService;
        this.notificationService = NotificationService;
        
        this.router = Router;
        this.bindingEngine = BindingEngine;

        const sub = this.bindingEngine.propertyObserver(this.authService, 'authenticated')
            .subscribe((newValue) => {
                this.isLoggedIn = newValue;
            });
    }

    logout() {
        this.usersService.logoutUser().then(() => {            
            this.notificationService.setProps({
                message: `You have been logged out`,
                isVisible: true
            });

            this.router.navigateToRoute('landing', { replace: true });
        });
    }
}
