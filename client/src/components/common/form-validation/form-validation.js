import { bindable } from 'aurelia-framework';

export class FormValidation {
    @bindable errorsCount

    errorsCountChanged(val) {
        setTimeout(() => {
            if (val) {
                this.disableButton();
            } else {
                this.enableButton();
            }
        });
    }

    disableButton() {
        this.submitBtn.classList.add('disabled');
        this.submitBtn.setAttribute('disabled', 'disabled');
    }

    enableButton() {
        this.submitBtn.classList.remove('disabled');
        this.submitBtn.removeAttribute('disabled');
    }
}
